# arranque de servidor

clonar el repositorio.
https://gitlab.com/daniel.gonzalez.guijarro/tecnoturist-back.git

ejecutar npm i

npm run dev... arranca el servicio de nodemon.

servidor arranca en localhost:5000

la bd para los usuarios en noSQL en MongoDB y está en la nube.

adjunto .env

# Funcionamiento.

    Muy básico. Loga y registra un usuario de acuerdo a las especificaciones de la prueba.
    No he llevado un control estricto de mensajes de errores. En 2 horas no da pa más.

# API.

    Hace el registro y el login del usuario y lo guarda todo en una BBDD en la nube.
    He metido la peticion a la API de Amadeus en el back porque en el front me daba muchos problemas de CORS y con el back todo va fluidito y bien.
    La API de Amadeus aunque funciona muy bien cuando la controlas, caduca cada media hora. o cada x peticiones no estoy seguro. Me obligaba a refresca el client_ID y El secret API constantemente, para conseguir el nuevo TOKEN. ¡Una locura!. Esto no pasa con la API de RIck and Morti jajajaja!!!.

    Aun así ha sido divertido, aunque se que presento un prueba técnica flojita, por no decir escasa ¡No pasa na! GRACIAS POR LA OPORTUNIDAD Y LES DESEO MUCHO EXITO!

    Daniel González.

