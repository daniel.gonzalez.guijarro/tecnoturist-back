const express = require('express');
const usersController = require('../controllers/users.controller');
const apiController = require('../controllers/api.controller');
const router = express.Router();

router.post('/login', usersController.loginPost);
router.post('/logout', usersController.logoutPost);
router.get('/check-session', usersController.checkSession);
router.post('/register', usersController.registerPost);
router.get('/hotel', apiController.getHotels);

module.exports = router;