const fetch = require('node-fetch');
require('dotenv').config();


module.exports = {
    getHotels: async (req, res, next) => {
      try{
        const request = await fetch("https://test.api.amadeus.com/v2/shopping/hotel-offers?cityCode=PAR", {
        method: "GET",
        headers:  {
          "Access-Control-Allow-Origin": "https://developers.amadeus.com",
          'Access-Control-Allow-Credentials': "true",
          "Authorization": process.env.TOKEN
        },
        credentials: 'include',
      })
      const response = await request.json();
      if(!request.ok) {
        return new Error(response.message);
      }
      return res.status(201).json(response)
    }catch(error){
      console.error(error.message);
   }
  }
} 
   
  